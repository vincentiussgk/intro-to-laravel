<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,
	initial-scale=1">
<title> Laman Signup Account
</title>
</head>
<body>
<h1> Buat Account Baru! </h1>
<h2> Sign Up Form </h2>
<form action = "/welcome" method = "post">
  @csrf

<label for="first_name"> First name:</label><br><br>
<input type="text" id="first_name" name="first_name"> <br><br>

<label for="last_name"> Last name: </label><br><br>
<input type="text" id="last_name" name="last_name"><br><br>

<p>Gender:</p>
<input type="radio" id="male" name="gender" value="male">
<label for="male">Male</label><br>
<input type="radio" id="female" name="gender" value="female">
<label for="female">Female</label><br>
<input type="radio" id="other" name="gender" value="other">
<label for="other">Other</label><br><br>

<label for="nationality">Nationality:</label><br><br>

<select id="nationality">
  <option value="Indonesian">Indonesian</option>
  <option value="Filipino">Filipino</option>
  <option value="Malaysian">Malaysian</option>
  <option value="Singaporean">Singaporean</option>
  <option value="Thai">Thai</option>
</select>

<p>Language Spoken:</p>
<input type="checkbox" id="indonesian" value="indonesian" name="indonesian_lang">
<label for="indonesian">Bahasa Indonesia</label><br>
<input type="checkbox" id="english" value="english" name="english">
<label for="english">English</label><br>
<input type="checkbox" id="other" value="other" name="other">
<label for="other">Other</label><br><br>

<label for="bio">Bio:</label><br>
<textarea name="bio" cols="30" rows="10"></textarea><br>

<input onclick="window.location.href='welcome.html';" type="submit" id="submit" value="Sign Up">
</form>
</body>
</html>
